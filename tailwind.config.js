/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {
        "2xl": "1280px",
      },
    },
    container: {
      center: true,
    },
  },
  plugins: [],
  prefix: "tw-",
};
