export default function fixNumber(
  num: number,
  decimalPosition?: number
): number {
  return Number((num || 0).toFixed(decimalPosition));
}
