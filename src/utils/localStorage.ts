export const getLocalStorage = (key) => {
  const data = localStorage.getItem(key);
  if (data) {
    try {
      JSON.parse(data);
    } catch (e) {
      return data;
    }
    return JSON.parse(localStorage.getItem(key));
  }
  return null;
};

export const setLocalStorage = (key, value) => {
  return localStorage.setItem(
    key,
    typeof value === "string" ? value : JSON.stringify(value)
  );
};

export const removeLocalStorage = (key) => {
  return localStorage.removeItem(key);
};
