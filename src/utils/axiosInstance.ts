import axios from "axios";
import queryString from "query-string";
import { authSliceActions } from "src/redux/auth/authSlice";
import store from "src/redux/rootReducer";
// import { getLocalStorage } from "src/utils/localStorage";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_API_URL,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
    "Access-Control-Allow-Headers":
      "Content-Type, Authorization, Content-Length, X-Requested-With",
  },
  paramsSerializer: {
    serialize: (params: any): string => {
      return queryString.stringify(params);
    },
  },
});

axiosInstance.interceptors.request.use(
  async (config) => {
    const accessToken = store.getState().auth.accessToken;
    if (accessToken && config && config.headers) {
      config.headers.Authorization = "Bearer " + accessToken;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error.response);
  }
);

axiosInstance.interceptors.response.use(
  (res) => res,
  async (err) => {
    console.log("error1:", err);
    if (err && err.response?.status === 403) {
      store.dispatch(authSliceActions.logout());
    }
    return Promise.reject(err.response);
  }
);

export default axiosInstance;
