export enum RouterPaths {
  LOGIN = "/login",
  REGISTER = "/register",
  HOME = "/",
  PROFILE = "/profile",
  SAVING_LIST = "/saving",
  DEMAND_DEPOSIT = "/saving-list/demand-deposit",
  TERM_SAVINGS_DEPOSIT = "/saving-list/term-savings-deposit",
  OPEN_PASSBOOK = "/open-passbook",
  FINALIZE_PASSBOOK = "/finalize-passbook",
}

export enum RequestStatus {
  PENDING = "LOADING",
  SUCCESS = "SUCCESS",
  ERROR = "ERROR",
}

export enum InterestPaymentType {
  ON_BALANCE = "OnBalance",
  ON_DEPOSIT_AMOUNT = "OnDepositAmount",
}

export const interestPaymentList = [
  {
    title: "Nhập lãi gốc",
    value: InterestPaymentType.ON_BALANCE,
  },
  {
    title: "Lãi trả vào tài khoản tiền gửi khi đến hạn trả lãi",
    value: InterestPaymentType.ON_DEPOSIT_AMOUNT,
  },
];
