import React from "react";
import { NavLink } from "react-router-dom";
import PageWrap from "src/components/PageWrap";
import { RouterPaths } from "src/constant";

const savingList = [
  {
    title: "Mở sổ tiết kiệm",
    link: RouterPaths.OPEN_PASSBOOK,
    img: "/images/open-passbook.png",
  },
  {
    title: "Tất toán tiền tiết kiệm",
    link: RouterPaths.FINALIZE_PASSBOOK,
    img: "/images/finalize-passbook.png",
  },
];

const SavingList = () => {
  return (
    <PageWrap title="Gửi tiền tiết kiệm">
      <div className="banner-wrap tw-relative">
        <img src="/images/savingBanner.jpg" alt="Gửi tiền tiết kiệm" />

        <div className="tw-absolute tw-w-full tw-h-full tw-top-0 ">
          <div className="tw-container tw-h-full tw-flex tw-items-center">
            <div className=" text-wrap tw-text-left ">
              <h3 className="tw-text-2xl tw-font-bold">Gửi tiết kiệm</h3>
              <span>
                Khách hàng có thể lựa chọn gửi tiết kiệm có thời hạn hoặc không
                có thời hạn
              </span>
            </div>
          </div>
        </div>
      </div>

      <div className="list-wrap tw-container">
        {savingList.map((item, index) => (
          <div className="item-wrap tw-flex tw-items-start tw-mt-8 tw-border-gray-100 tw-bg-gray-100 tw-p-6 tw-rounded">
            <div
              className="img-wrap tw-w-[400px] tw-h-[250px] tw-bg-cover tw-rounded"
              style={{ backgroundImage: `url('${item.img}')` }}></div>

            <div className="tw-text-left tw-ml-8">
              <NavLink to={item.link}>
                <h3 className="tw-text-xl tw-font-bold tw-underline">
                  {item.title}
                </h3>
              </NavLink>
            </div>
          </div>
        ))}
      </div>
    </PageWrap>
  );
};

export default SavingList;
