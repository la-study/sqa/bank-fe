import { Button, Divider, Form, Input } from "antd";
import React, { useEffect } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { RouterPaths } from "src/constant";
import { authSliceActions } from "src/redux/auth/authSlice";
import {
  RootState,
  useAppDispatch,
  useAppSelector,
} from "src/redux/rootReducer";
import axiosInstance from "src/utils/axiosInstance";
import useDocumentTitle from "src/utils/useDocumentTitle";

const LoginPage: React.FC = () => {
  const { accessToken } = useAppSelector((state: RootState) => state.auth);
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const onFinish = async (values: any) => {
    try {
      const response = await axiosInstance.post("/auth/authenticate", values);
      dispatch(authSliceActions.loginSuccess(response.data));
      navigate(RouterPaths.HOME);
    } catch (error) {}
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    if (accessToken) {
      navigate(RouterPaths.HOME);
    }
  }, [accessToken, navigate]);

  useDocumentTitle({ title: "Login" });

  return (
    <div className="login-page tw-flex tw-h-[100vh] tw-justify-center tw-items-center tw-flex-col">
      <h3 className="tw-text-xl tw-font-semibold tw-uppercase tw-mb-5">
        Đăng nhập
      </h3>
      <Form
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
        autoComplete="off"
        className="tw-w-[26rem] tw-max-w-[95vw]">
        <Form.Item
          label="Username"
          name="username"
          initialValue="justmegiang@gmail.com"
          rules={[{ required: true, message: "Please input your username!" }]}>
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          initialValue="123qweA@"
          rules={[{ required: true, message: "Please input your password!" }]}>
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>

        <Divider />

        <div className="tw-flex tw-items-center tw-justify-center">
          <span className="tw-mr-2">Chưa có tài khoản</span>
          <NavLink to={RouterPaths.REGISTER}>Đăng ký</NavLink>
        </div>
      </Form>
    </div>
  );
};

export default LoginPage;
