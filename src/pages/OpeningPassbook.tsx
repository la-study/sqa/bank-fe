import { Steps } from "antd";
import React, { useEffect, useState } from "react";
import PageWrap from "src/components/PageWrap";
import Confirmation from "src/components/openPassbook/Confirmation";
import Initialization from "src/components/openPassbook/Initialization";
import Result from "src/components/openPassbook/Result";
import { IDepositInfo } from "src/types";
import axiosInstance from "src/utils/axiosInstance";

const OpeningPassbook = () => {
  const [currentStep, setCurrentStep] = React.useState(0);
  const [data, setData] = useState<IDepositInfo>({
    depositAmount: undefined,
    interestPaymentType: undefined,
    term: undefined,
  });

  const [termsList, setTermsList] = useState([]);

  const steps = [
    {
      title: "Khởi tạo",
      key: "Khởi tạo",
      content: (
        <Initialization
          termsList={termsList}
          data={data}
          setData={setData}
          setCurrentStep={setCurrentStep}
        />
      ),
    },
    {
      title: "Xác nhận",
      key: "",
      content: (
        <Confirmation
          setData={setData}
          data={data}
          termsList={termsList}
          setCurrentStep={setCurrentStep}
        />
      ),
    },
    {
      title: "Kết quả",
      key: "Kết quả",
      content: (
        <Result data={data} setData={setData} setCurrentStep={setCurrentStep} />
      ),
    },
  ];

  React.useEffect(() => {
    const getTermsList = async () => {
      try {
        const response = await axiosInstance.get("/api/interest_rates");
        setTermsList(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    getTermsList();
  }, []);

  return (
    <PageWrap title="Mở sổ tiết kiệm">
      <h3 className="tw-text-2xl tw-font-bold tw-uppercase tw-mt-8">
        Mở sổ tiết kiệm
      </h3>
      <div className="tw-container">
        <div className="tw-w-[36rem] tw-mx-auto">
          <Steps
            current={currentStep}
            items={steps.map((item) => ({
              key: item.key,
              title: item.title,
            }))}
          />
        </div>
        <div className="content-wrap">{steps[currentStep]?.content}</div>
      </div>
    </PageWrap>
  );
};

export default OpeningPassbook;
