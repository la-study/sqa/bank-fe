import React from "react";
import PageWrap from "src/components/PageWrap";
import { RootState, useAppSelector } from "src/redux/rootReducer";
import fixNumber from "src/utils/fixNumber";

const Profile = () => {
  const { currentUser } = useAppSelector((state: RootState) => state.auth);

  return (
    <PageWrap title="Thông tin cá nhân">
      <h3 className="tw-text-2xl tw-font-bold tw-uppercase tw-mt-8">
        Thông tin cá nhân
      </h3>
      <div className="initialization-wrap">
        <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
          <div className="tw-my-6 tw-bg-slate-100   tw-p-6 tw-rounded-md">
            <div className="form-wrap tw-flex tw-text-left ">
              <div className="label tw-w-[120px]">Họ tên: </div>
              <span className="tw-ml-3">{currentUser?.user?.name}</span>
            </div>{" "}
            <div className="form-wrap tw-flex tw-text-left ">
              <div className="label tw-w-[120px]">Email: </div>
              <span className="tw-ml-3">{currentUser?.user?.email}</span>
            </div>{" "}
            <div className="form-wrap tw-flex tw-text-left ">
              <div className="label tw-w-[120px]">Số tài khoản: </div>
              <span className="tw-ml-3">{currentUser?.user?.phone_number}</span>
            </div>
            <div className="form-wrap tw-flex tw-text-left  ">
              <div className="label tw-w-[120px]">Số dư khả dụng: </div>
              <span className="tw-ml-3">
                {new Intl.NumberFormat("vi-VN", {
                  currency: "VND",
                }).format(fixNumber(currentUser?.balance?.balance))}
                {" VND"}
              </span>
            </div>
          </div>
        </div>
      </div>
    </PageWrap>
  );
};

export default Profile;
