import { Steps } from "antd";
import React, { useState } from "react";
import PageWrap from "src/components/PageWrap";
import Confirmation from "src/components/finalizePassbook/Confirmation";
import Initialization from "src/components/finalizePassbook/Initialization";
import Result from "src/components/finalizePassbook/Result";
import axiosInstance from "src/utils/axiosInstance";

const FinalizePassbook = () => {
  const [currentStep, setCurrentStep] = React.useState(0);

  const [data, setData] = useState<any>(null);
  const [passbooksList, setPassbooksList] = useState([]);
  const steps = [
    {
      title: "Khởi tạo",
      key: "Khởi tạo",
      content: (
        <Initialization
          data={data}
          setCurrentStep={setCurrentStep}
          passbooksList={passbooksList}
          setData={setData}
        />
      ),
    },
    {
      title: "Xác nhận",
      key: "Xác nhận",
      content: (
        <Confirmation
          setData={setData}
          data={data}
          setCurrentStep={setCurrentStep}
        />
      ),
    },
    {
      title: "Kết quả",
      key: "Kết quả",
      content: (
        <Result data={data} setData={setData} setCurrentStep={setCurrentStep} />
      ),
    },
  ];

  React.useEffect(() => {
    const getPassbooksList = async () => {
      try {
        const response = await axiosInstance.get("/api/savingbook");
        setPassbooksList(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    if (currentStep === 0) {
      getPassbooksList();
    }
  }, [currentStep]);

  return (
    <PageWrap title="Tất toán sổ tiết kiệm">
      <h3 className="tw-text-2xl tw-font-bold tw-uppercase tw-mt-8">
        Rút sổ tiết kiệm
      </h3>
      <div className="tw-container">
        <div className="tw-w-[36rem] tw-mx-auto">
          <Steps
            current={currentStep}
            items={steps.map((item) => ({
              key: item.key,
              title: item.title,
            }))}
          />
        </div>
        <div className="content-wrap">{steps[currentStep]?.content}</div>
      </div>
    </PageWrap>
  );
};

export default FinalizePassbook;
