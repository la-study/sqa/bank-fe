import React from "react";

import { Button, Result } from "antd";
import { Link } from "react-router-dom";
import { RouterPaths } from "src/constant";
import useDocumentTitle from "src/utils/useDocumentTitle";

const NotFound = () => {
  useDocumentTitle({ title: "404 Not Found" });

  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={
        <Button type="primary">
          <Link to={RouterPaths.HOME}> Quay lại trang chủ</Link>
        </Button>
      }
    />
  );
};

export default NotFound;
