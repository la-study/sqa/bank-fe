import { Button, Divider, Form, Input } from "antd";
import React from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { RouterPaths } from "src/constant";
import { useAppDispatch } from "src/redux/rootReducer";
import axiosInstance from "src/utils/axiosInstance";

const Register = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();

  const onFinish = async (values: any) => {
    try {
      const response = await axiosInstance.post("/auth/register", values);
      navigate(RouterPaths.HOME);
    } catch (error) {}
  };

  return (
    <div className="login-page tw-flex tw-h-[100vh] tw-justify-center tw-items-center tw-flex-col">
      <h3 className="tw-text-xl tw-font-semibold tw-uppercase tw-mb-5">
        Đăng ký
      </h3>
      <Form
        name="basic"
        onFinish={onFinish}
        layout="vertical"
        autoComplete="off"
        className="tw-w-[26rem] tw-max-w-[95vw]">
        <Form.Item
          label="Họ tên"
          name="name"
          initialValue=""
          rules={[{ required: true, message: "Vui lòng nhập họ tên" }]}>
          <Input placeholder="Nhập họ tên" />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Vui lòng nhập email" }]}>
          <Input placeholder="Nhập email" />
        </Form.Item>

        <Form.Item
          label="Số điện thoại"
          name="phone_number"
          rules={[{ required: true, message: "Vui lòng nhập số điện thoại" }]}>
          <Input placeholder="Nhập số điện thoại" />
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[{ required: true, message: "Vui lòng nhập mật khẩu" }]}>
          <Input.Password placeholder="Nhập mật khẩu" />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Đăng ký
          </Button>
        </Form.Item>

        <Divider />

        <div className="tw-flex tw-items-center tw-justify-center">
          <span className="tw-mr-2">Đã có tài khoản</span>
          <NavLink to={RouterPaths.LOGIN}>Đăng nhập</NavLink>
        </div>
      </Form>
    </div>
  );
};

export default Register;
