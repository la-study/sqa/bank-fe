import React from "react";
import PageWrap from "src/components/PageWrap";

const HomePage = () => {
  return <PageWrap title="Trang chủ">Trang chủ</PageWrap>;
};

export default HomePage;
