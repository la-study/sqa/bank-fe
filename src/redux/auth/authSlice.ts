import { createSlice } from "@reduxjs/toolkit";
import {
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
} from "src/utils/localStorage";

const initialState = {
  accessToken: getLocalStorage("accessToken"),
  currentUser: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    loginSuccess(state, action) {
      const accessToken = action.payload.token;
      state.accessToken = accessToken;
      setLocalStorage("accessToken", accessToken);
    },

    getCurrentUser(state, action) { 
      const currentUser = action.payload;
      state.currentUser = currentUser;
    },

    logout(state) {
      state.accessToken = "";
      removeLocalStorage("accessToken");
    },

    loginFail(state, action) {},
  },
});
export const authReducer = authSlice.reducer;

export const authSliceActions = authSlice.actions;
