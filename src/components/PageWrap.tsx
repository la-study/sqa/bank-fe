import { UserOutlined } from "@ant-design/icons";
import { Avatar, Dropdown, Layout, Menu } from "antd";
import React, { useEffect } from "react";
import { NavLink } from "react-router-dom";
import { RouterPaths } from "src/constant";
import { authSliceActions } from "src/redux/auth/authSlice";
import {
  RootState,
  useAppDispatch,
  useAppSelector,
} from "src/redux/rootReducer";
import axiosInstance from "src/utils/axiosInstance";
import useDocumentTitle from "src/utils/useDocumentTitle";

const { Header, Content } = Layout;

interface IPageWrapProps {
  title: string;
  children?: string | JSX.Element | JSX.Element | JSX.Element[];
}

const PageWrap = ({ title, children }: IPageWrapProps) => {
  useDocumentTitle({ title });
  const { accessToken } = useAppSelector((state: RootState) => state.auth);

  const dispatch = useAppDispatch();

  const handleLogout = () => {
    dispatch(authSliceActions.logout());
  };

  useEffect(() => {
    const getCurrentUser = async () => {
      try {
        const resUser = await axiosInstance.get("api/user-info");
        dispatch(authSliceActions.getCurrentUser(resUser.data));
      } catch (error) {
        console.log("Error get user: ", error);
      }
    };

    getCurrentUser();
  }, [dispatch]);

  return (
    <Layout className="tw-h-screen">
      <Header>
        <div className="tw-container tw-m-auto tw-h-full">
          <div className="tw-flex tw-justify-between tw-items-center tw-h-full">
            {/* <div className="left-wrap">
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: () => setCollapsed(!collapsed),
                }
              )}
            </div> */}
            <div className="logo-wrap tw-h-[50px]">
              <NavLink to={RouterPaths.HOME}>
                <img
                  src="/images/logo_full.png"
                  alt="Logo"
                  className="tw-h-full"
                />
              </NavLink>
            </div>

            <div className="right-wrap tw-flex tw-items-center">
              <NavLink to={RouterPaths.OPEN_PASSBOOK} className="tw-mx-3">
                Mở sổ tiết kiệm
              </NavLink>
              <NavLink to={RouterPaths.FINALIZE_PASSBOOK} className="tw-mx-3">
                Tất toán tiền tiết kiệm
              </NavLink>

              <Dropdown
                trigger={["click"]}
                overlay={
                  <Menu
                    items={[
                      {
                        label: (
                          <NavLink to={RouterPaths.PROFILE}>
                            Thông tin cá nhân
                          </NavLink>
                        ),
                        key: "1",
                      },
                      {
                        label: (
                          <NavLink to="" onClick={handleLogout}>
                            Logout
                          </NavLink>
                        ),
                        key: "2",
                      },
                    ]}
                  />
                }>
                {accessToken && (
                  <div className="user-wrap tw-ml-2">
                    <Avatar
                      size={36}
                      icon={<UserOutlined />}
                      className="tw-cursor-pointer"
                    />
                  </div>
                )}
              </Dropdown>
            </div>
          </div>
        </div>
      </Header>

      <Content id="main-content">{children}</Content>
    </Layout>
  );
};

export default PageWrap;
