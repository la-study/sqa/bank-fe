import { Button } from "antd";
import moment from "moment";
import React from "react";
import { interestPaymentList } from "src/constant";
import axiosInstance from "src/utils/axiosInstance";
import fixNumber from "src/utils/fixNumber";

interface InitializationProps {
  data: any;
  setData: React.Dispatch<React.SetStateAction<any>>;
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>;
}

const Confirmation = ({
  data,
  setData,
  setCurrentStep,
}: InitializationProps) => {
  const handleConfirm = async () => {
    try {
      console.log("data: ", data);
      const response = await axiosInstance.post("/api/savingbook/width-draw", {
        id: data.id,
        depositAmount: data.depositAmount,
      });
      console.log("response: ", response.data);
      setCurrentStep(2);
      setData(response.data);
    } catch (error) {
      console.log("error confirm: ", error);
    }
  };
  console.log("data confirm withdraw: ", data);

  return (
    <div className="confirmation-wrap">
      <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
        <div className="tw-my-6 tw-bg-slate-100 tw-px-6 tw-py-3 tw-rounded-md">
          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Tài khoản tiết kiệm: </div>
            <span className="tw-ml-3"> {data.id} </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Tên tài khoản: </div>
            <span className="tw-ml-3"> {data.user.name} </span>
          </div>
          {/* <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Kỳ hạn gửi: </div>
            <span className="tw-ml-3">{`${data?.term} tháng`}</span>
          </div> */}

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Thời gian gửi: </div>
            <span className="tw-ml-3">
              {moment(data?.createdAt).format("hh:mm DD-MM-YYYY")}
            </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Thời gian tới hạn: </div>
            <span className="tw-ml-3">
              {moment(data?.createdAt)
                .add(data.interestRate.term, "M")
                .format("hh:mm DD-MM-YYYY")}
            </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Số tiền gửi:</div>
            <span className="tw-ml-3">
              {" "}
              {new Intl.NumberFormat("vi-VN", {
                currency: "VND",
              }).format(fixNumber(data.depositAmount))}{" "}
              VND
            </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-text-left">Hình thức trả lãi:</div>
            <span className="tw-ml-3">
              {
                interestPaymentList.find(
                  (item) => item.value === data.interestPaymentType
                )?.title
              }
            </span>
          </div>
        </div>

        <div className="tw-flex tw-justify-center">
          <Button
            className="tw-mx-3"
            type="default"
            htmlType="submit"
            onClick={() => {
              setCurrentStep(0);
            }}>
            Quay lại
          </Button>

          <Button
            className="tw-mx-3"
            type="primary"
            htmlType="submit"
            onClick={handleConfirm}>
            Tiếp tục
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Confirmation;
