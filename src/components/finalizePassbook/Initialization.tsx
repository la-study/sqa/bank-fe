import { Button, Form, Select } from "antd";
import React, { useState } from "react";
import { IDepositInfo } from "src/types";
import fixNumber from "src/utils/fixNumber";

const { Option } = Select;

interface InitializationProps {
  data: any;
  passbooksList: any[];
  setData: React.Dispatch<React.SetStateAction<IDepositInfo>>;
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>;
}

const Initialization = ({
  data,
  passbooksList,
  setData,
  setCurrentStep,
}: InitializationProps) => {
  const [form] = Form.useForm();
  const [selectedPassbook, setSelectedPassbook] = useState<any>(null);

  const onFinish = (values: any) => {
    setData(passbooksList.find((item) => item.id === values.passbook));
    setCurrentStep(1);
  };

  const handleSelectPassbook = (value, option) => {
    setSelectedPassbook(passbooksList.find((item) => item.id === value));
  };

  return (
    <div className="initialization-wrap">
      <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
        <Form
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          layout="vertical">
          <div className="tw-my-6 tw-bg-slate-100 tw-p-6 tw-rounded-md">
            <Form.Item
              name="passbook"
              label="Tài khoản tiết kiệm"
              rules={[
                {
                  required: true,
                  message: "Vui lòng chọn tài khoản tiết kiệm",
                },
              ]}>
              <Select
                placeholder="Chọn tài khoản tiết kiệm"
                className="tw-text-left"
                onChange={handleSelectPassbook}
                allowClear>
                {passbooksList.map((item) => (
                  <Option key={item.id} value={item?.id}>
                    {item.name || `Tài khoản ${item.id}`}
                  </Option>
                ))}
              </Select>
            </Form.Item>

            {selectedPassbook && (
              <div className="form-wrap tw-flex tw-text-left  ">
                <div className="label tw-w-[120px]">Số dư thực nhận: </div>
                <span className="tw-ml-3">
                  {new Intl.NumberFormat("vi-VN", {
                    currency: "VND",
                  }).format(fixNumber(selectedPassbook?.depositAmount))}
                  {" VND"}
                </span>
              </div>
            )}

            <Form.Item>
              <Button className="tw-mt-4" type="primary" htmlType="submit">
                Tiếp tục
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Initialization;
