import { CheckCircleOutlined } from "@ant-design/icons";
import { Button } from "antd";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { RouterPaths } from "src/constant";
import { RootState, useAppSelector } from "src/redux/rootReducer";
import { IDepositInfo } from "src/types";
import fixNumber from "src/utils/fixNumber";
interface ResultProps {
  data: IDepositInfo & any;
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>;
  setData: React.Dispatch<React.SetStateAction<IDepositInfo>>;
}

const Result = ({ data, setCurrentStep, setData }: ResultProps) => {
  const navigate = useNavigate();
  const [test, setTest] = useState("");
  const { currentUser } = useAppSelector((state: RootState) => state.auth);

  const handleCreateNewTransaction = () => {
    setCurrentStep(0);
    setData({
      depositAmount: undefined,
      interestPaymentType: undefined,
      term: undefined,
    });
  };

  return (
    <div className="result-wrap">
      <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
        <div className="tw-my-6 tw-bg-slate-100 tw-px-6 tw-py-3 tw-rounded-md">
          <CheckCircleOutlined
            style={{
              fontSize: 48,
              color: "#78b840",
            }}
            className="tw-mt-8 tw-mb-4"
          />

          <div className="tw-text-lg tw-font-bold tw-uppercase">
            Giao dịch thành công
          </div>

          <div className="tw-text-xl tw-font-bold tw-text-[#78b840] tw-uppercase">
            {new Intl.NumberFormat("vi-VN", {
              currency: "VND",
            }).format(fixNumber(data.depositAmount))}{" "}
            VND
          </div>

          <div className="">
            {moment(data.createdAt)
              .locale(["vi"])
              .format("hh:mm dddd DD/MM/YYYY")}
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">
              Số tài khoản:{" "}
            </div>
            <span className="tw-ml-3">{currentUser?.user?.phone_number}</span>
          </div>
        </div>

        <div className="tw-flex tw-justify-center">
          <Button
            className="tw-mx-3"
            type="default"
            htmlType="submit"
            onClick={() => {
              navigate(RouterPaths.HOME);
            }}>
            Về trang chủ
          </Button>

          <Button
            className="tw-mx-3"
            type="primary"
            htmlType="submit"
            onClick={handleCreateNewTransaction}>
            Tạo giao dịch mới
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Result;
