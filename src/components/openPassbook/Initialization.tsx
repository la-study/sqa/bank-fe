import { Button, Form, Input, Radio, Select } from "antd";
import React, { useEffect, useState } from "react";
import { interestPaymentList } from "src/constant";
import { RootState, useAppSelector } from "src/redux/rootReducer";
import { IDepositInfo } from "src/types";
import fixNumber from "src/utils/fixNumber";

const { Option } = Select;

interface InitializationProps {
  data: IDepositInfo;
  termsList: any[];
  setData: React.Dispatch<React.SetStateAction<IDepositInfo>>;
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>;
}

const Initialization = ({
  data,
  termsList,
  setData,
  setCurrentStep,
}: InitializationProps) => {
  const [form] = Form.useForm();
  const [isChecked, setIsChecked] = useState(false);
  const [isSubmited, setIsSubmited] = useState(false);
  const { currentUser } = useAppSelector((state: RootState) => state.auth);

  const onFinish = (values: any) => {
    setIsSubmited(true);
    const newData = {
      term: values?.term,
      rating: termsList.find((item) => item.term === values.term),
      depositAmount: Number(values.depositAmount),
      interestPaymentType: values.interestPaymentType,
    };
    setData(newData);
    if (isChecked) {
      setCurrentStep(1);
    }
  };

  const handleCheck = (event) => {
    setIsChecked(event.target.checked);
  };

  useEffect(() => {
    form.setFieldsValue({
      term: data?.term || undefined,
      depositAmount: data.depositAmount,
      interestPaymentType: data.interestPaymentType,
    });
  }, [data, termsList, form]);

  return (
    <div className="initialization-wrap">
      <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
        <Form
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          layout="vertical">
          <div className="tw-my-6 tw-bg-slate-100   tw-p-6 tw-rounded-md">
            <div className="form-wrap tw-flex tw-text-left ">
              <div className="label tw-w-[120px]">Số tài khoản: </div>
              <span className="tw-ml-3">{currentUser?.user?.phone_number}</span>
            </div>
            <div className="form-wrap tw-flex tw-text-left  ">
              <div className="label tw-w-[120px]">Số dư khả dụng: </div>
              <span className="tw-ml-3">
                {new Intl.NumberFormat("vi-VN", {
                  currency: "VND",
                }).format(fixNumber(currentUser?.balance?.balance))}
                {" VND"}
              </span>
            </div>
          </div>

          <div className="tw-my-6 tw-bg-slate-100   tw-p-6 tw-rounded-md">
            <Form.Item
              initialValue={data?.term}
              name="term"
              label="Kỳ hạn gửi"
              rules={[{ required: true, message: "Vui lòng chọn kỳ hạn" }]}>
              <Select
                placeholder="Chọn kỳ hạn gửi"
                className="tw-text-left"
                allowClear>
                {termsList.map((item) => (
                  <Option key={item.id} value={item?.term}>
                    {`${item?.term} Tháng (${item?.rate}%)`}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              name="depositAmount"
              label="Số tiền gửi"
              initialValue={data.depositAmount}
              rules={[
                { required: true, message: "Vui lòng nhập số tiền gửi" },
              ]}>
              <Input
                placeholder="Nhập số tiền gửi"
                suffix="VND"
                type="number"
              />
            </Form.Item>
            <Form.Item
              name="interestPaymentType"
              label="Hình thức trả lãi"
              initialValue={data.interestPaymentType}
              rules={[
                { required: true, message: "Vui lòng chọn hình thức trả lãi" },
              ]}>
              <Select
                placeholder="Chọn hình thức trả lãi"
                className="tw-text-left"
                allowClear>
                {interestPaymentList.map((item, index) => (
                  <Option key={index} value={item.value}>
                    {item.title}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="isChecked">
              <Radio
                name="isChecked"
                value={isChecked}
                onChange={handleCheck}
                className="tw-text-left">
                Tôi đã đọc, hiểu rõ, đồng ý và cam kết tuân thủ các điều khoản,
                điều kiện về{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://digibankm5.vietcombank.com.vn/get_file/ibomni/html/thoathuan-movasudung-tienguilinhhoat.html">
                  Thỏa thuận mở và sử dụng tiền gửi có kỳ hạn trên kênh NHĐT
                </a>{" "}
                và{" "}
                <a
                  target="_blank"
                  rel="noreferrer"
                  href="https://digibankm5.vietcombank.com.vn/get_file/ibomni/html/dieukhoan-tien-gui-linhhoat.html">
                  Điều khoản cơ bản quy định sản phẩm Tiền gửi trực tuyến
                </a>{" "}
                của Ngân hàng TMCP Ngoại thương Việt Nam (Vietcombank).
              </Radio>

              {!isChecked && isSubmited && (
                <div className="ant-form-item-explain-error">
                  Vui lòng đọc và xác nhận về điều khoản dịch vụ
                </div>
              )}
            </Form.Item>
            <Form.Item>
              <Button className="tw-mt-4" type="primary" htmlType="submit">
                Tiếp tục
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </div>
  );
};

export default Initialization;
