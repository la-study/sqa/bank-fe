import { Button } from "antd";
import React from "react";
import { InterestPaymentType, interestPaymentList } from "src/constant";
import { RootState, useAppSelector } from "src/redux/rootReducer";
import { IDepositInfo } from "src/types";
import axiosInstance from "src/utils/axiosInstance";
import fixNumber from "src/utils/fixNumber";
import { setLocalStorage } from "src/utils/localStorage";

interface InitializationProps {
  data: IDepositInfo & any;
  termsList: any[];
  setData: React.Dispatch<React.SetStateAction<IDepositInfo & any>>;
  setCurrentStep: React.Dispatch<React.SetStateAction<number>>;
}

const Confirmation = ({
  data,
  termsList,
  setData,
  setCurrentStep,
}: InitializationProps) => {
  const { currentUser } = useAppSelector((state: RootState) => state.auth);

  const handleConfirm = async () => {
    try {
      const postData = {
        savingBookName: Date.now().toString(),
        interestPaymentType: data.interestPaymentType,
        depositAmount: data.depositAmount,
        interestRateId: data.rating.id,
      };
      const response = await axiosInstance.post("/api/savingbook", postData);

      setCurrentStep(2);

      setData(response.data);
      setLocalStorage("depositInfo", response.data);
    } catch (error) {
      console.log("error confirm: ", error);
    }
  };

  const calculateInterest = (deposit, rating) => {
    const result = fixNumber(
      deposit * (1 + (rating.rate / 100) * (rating.term / 12))
    );

    return result;
  };

  console.log("data confirm open: ", data);

  return (
    <div className="confirmation-wrap">
      <div className="form-wrap tw-max-w-[94vw] tw-w-[36rem] tw-mx-auto">
        <div className="tw-my-6 tw-bg-slate-100 tw-px-6 tw-py-3 tw-rounded-md">
          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">
              Số tài khoản:{" "}
            </div>
            <span className="tw-ml-3">{currentUser?.user?.phone_number}</span>
          </div>
        </div>

        <div className="tw-my-6 tw-bg-slate-100 tw-px-6 tw-py-3 tw-rounded-md">
          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">Kỳ hạn gửi: </div>
            <span className="tw-ml-3">{`${data?.rating.term} tháng`}</span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">Lãi suất: </div>
            <span className="tw-ml-3">{`${
              termsList.find((item) => item?.term === data?.rating.term)?.rate
            } %/năm`}</span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">Số tiền gửi:</div>
            <span className="tw-ml-3">
              {" "}
              {new Intl.NumberFormat("vi-VN", {
                currency: "VND",
              }).format(fixNumber(data.depositAmount))}{" "}
              VND
            </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">
              Số tiền sau kì hạn:
            </div>
            <span className="tw-ml-3">
              {" "}
              {new Intl.NumberFormat("vi-VN", {
                currency: "VND",
              }).format(
                calculateInterest(data.depositAmount, data.rating)
              )}{" "}
              VND
            </span>
          </div>

          <div className="form-wrap tw-flex tw-justify-between tw-my-3">
            <div className="label tw-w-[120px] tw-text-left">
              Hình thức trả lãi:
            </div>
            <span className="tw-ml-3">
              {
                interestPaymentList.find(
                  (item) => item.value === data.interestPaymentType
                )?.title
              }
            </span>
          </div>
        </div>

        <div className="tw-flex tw-justify-center">
          <Button
            className="tw-mx-3"
            type="default"
            htmlType="submit"
            onClick={() => {
              setCurrentStep(0);
            }}>
            Quay lại
          </Button>

          <Button
            className="tw-mx-3"
            type="primary"
            htmlType="submit"
            onClick={handleConfirm}>
            Tiếp tục
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Confirmation;
