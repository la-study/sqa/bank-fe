import { InterestPaymentType } from "src/constant";

export interface IDepositInfo {
  depositAmount: number;
  interestPaymentType: InterestPaymentType;
  term: number;
}
