import React from "react";
import { BrowserRouter } from "react-router-dom";
import "./scss/App.scss";
import "antd/dist/antd.min.css";
import MainRoutes from "./routes/MainRoutes";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <MainRoutes />
      </div>
    </BrowserRouter>
  );
}

export default App;
