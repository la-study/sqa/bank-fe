import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { RootState, useAppSelector } from "src/redux/rootReducer";

const PrivatedRoute = () => {
  const { accessToken } = useAppSelector((state: RootState) => state.auth);

  return (
    <>{!accessToken ? <Navigate to="/login" replace={true} /> : <Outlet />}</>
  );
};

export default PrivatedRoute;
