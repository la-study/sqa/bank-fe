import React, { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import Loading from "src/components/Loading";
import { RouterPaths } from "src/constant";
import FinalizePassbook from "src/pages/FinalizePassbook";
import Homepage from "src/pages/Homepage";
import LoginPage from "src/pages/Login";
import NotFound from "src/pages/NotFound";
import OpeningPassbook from "src/pages/OpeningPassbook";
import Register from "src/pages/Register";
import SavingList from "src/pages/SavingList";
import PrivatedRoute from "./PrivatedRoute";
import Profile from "src/pages/Profile";

const MainRoutes = () => {
  return (
    <Suspense fallback={<Loading />}>
      <Routes>
        <Route path={RouterPaths.LOGIN} element={<LoginPage />} />
        <Route path={RouterPaths.REGISTER} element={<Register />} />
        <Route path="" element={<PrivatedRoute />}>
          <Route path={RouterPaths.HOME} element={<Homepage />} />
          <Route path={RouterPaths.SAVING_LIST} element={<SavingList />} />
          <Route
            path={RouterPaths.OPEN_PASSBOOK}
            element={<OpeningPassbook />}
          />
          <Route
            path={RouterPaths.FINALIZE_PASSBOOK}
            element={<FinalizePassbook />}
          />
          <Route path={RouterPaths.PROFILE} element={<Profile />} />
        </Route>
        <Route path="/*" element={<NotFound />} />
      </Routes>
    </Suspense>
  );
};

export default MainRoutes;
